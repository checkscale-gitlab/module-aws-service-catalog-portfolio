variable "name" {
  description = "Name of the portfolio."
  default     = "portfolio"
  type        = string
}

variable "description" {
  description = "Description of the portfolio."
  default     = null
  type        = string
}

variable "provider_name" {
  description = "Name of the provider of the portfolio."
  default     = null
  type        = string
}

variable "tags" {
  description = "Map of tags that will be applied on all the resources."
  default     = {}
  type        = map(string)
}

variable "portfolio_shares" {
  description = <<DESC
Map of shares for the portfolio following:
{
  foo = {
    principal_id        = string
    type                = string
    accept_language     = optional(string)
    share_tag_options   = optional(bool)
    wait_for_acceptance = optional(bool)

  }
}
DESC
  default     = {}
  type        = map(map(string))
}

variable "portfolio_principals" {
  description = <<DESD
  Map of principals associated to the portfolio, following this structure:
  {
    foo = {
      principal_arn   = string
      accept_language = optional(string)
      principal_type  = optional(string)
    }
  }
  DESD
  default     = {}
  type        = map(map(string))
}
